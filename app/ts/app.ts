import '../scss/styles';
import * as mapboxgl from 'mapbox-gl';

(mapboxgl as any).accessToken = '';
let map: mapboxgl.Map;
map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9'
});